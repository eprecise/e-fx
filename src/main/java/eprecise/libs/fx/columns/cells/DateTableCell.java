
package eprecise.libs.fx.columns.cells;

import java.text.DateFormat;
import java.util.Date;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.TableCell;


public class DateTableCell<T> extends TableCell<T, Date> {

    private final ObjectProperty<DateFormat> formatProperty = new SimpleObjectProperty<>(DateFormat.getDateTimeInstance());

    @Override
    protected void updateItem(Date item, boolean empty) {
        super.updateItem(item, empty);
        if (item == null || empty) {
            this.setText(null);
        } else {
            this.setText(this.formatProperty.get().format(item));
        }
    }
}


package eprecise.libs.fx.fields.number;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.function.Function;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;


public class NumberField<T extends Number> extends Control {

    private final ObjectProperty<NumberFormat> formatProperty = new SimpleObjectProperty<>(DecimalFormat.getNumberInstance());

    private final ObjectProperty<T> valueProperty = new SimpleObjectProperty<>();

    @SuppressWarnings("unchecked")
    private final ObjectProperty<Function<Number, T>> numberConverterProperty = new SimpleObjectProperty<>(n -> (T) n);

    private final BooleanProperty editableProperty = new SimpleBooleanProperty(true);

    public T getValue() {
        return this.valueProperty.get();
    }

    public void setValue(T value) {
        this.valueProperty.set(value);
    }

    public ObjectProperty<T> valueProperty() {
        return this.valueProperty;
    }

    public NumberFormat getFormat() {
        return this.formatProperty.get();
    }

    public void setFormat(NumberFormat value) {
        this.formatProperty.set(value);
    }

    public ObjectProperty<NumberFormat> formatProperty() {
        return this.formatProperty;
    }

    public Function<Number, T> getNumberConverter() {
        return this.numberConverterProperty.get();
    }

    public void setNumberConverter(Function<Number, T> converter) {
        this.numberConverterProperty.set(converter);
    }

    public ObjectProperty<Function<Number, T>> numberConverterProperty() {
        return this.numberConverterProperty;
    }

    public boolean isEditable() {
        return this.editableProperty.get();
    }

    public void setEditable(boolean value) {
        this.editableProperty.set(value);
    }

    public BooleanProperty editableProperty() {
        return this.editableProperty;
    }

    @Override
    protected Skin<?> createDefaultSkin() {
        return new NumberFieldSkin<>(this);
    }
}

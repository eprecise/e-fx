
package eprecise.libs.fx.fields.number;

import static org.apache.commons.lang3.StringUtils.isEmpty;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.util.Optional;

import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.control.SkinBase;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;


public class NumberFieldSkin<T extends Number> extends SkinBase<NumberField<T>> {

    private final TextField field = new TextField();

    private boolean silent = false;

    protected NumberFieldSkin(NumberField<T> control) {
        super(control);
        this.field.editableProperty().bind(control.editableProperty());
        this.field.setTextFormatter(new TextFormatter<>(c -> {
            if (c.getControlNewText().isEmpty()) {
                return c;
            }

            final ParsePosition parsePosition = new ParsePosition(0);
            final Object object = this.getFormat().parse(c.getControlNewText(), parsePosition);

            if (object == null || parsePosition.getIndex() < c.getControlNewText().length()) {
                return null;
            } else {
                return c;
            }
        }));
        control.valueProperty().addListener(i -> this.updateFieldFromValue());
        control.numberConverterProperty().addListener(i -> this.updateFieldFromValue());
        this.field.focusedProperty().addListener((ob, o, n) -> {
            if (!n) {
                try {
                    this.updateValueFromField();
                } catch (final ParseException e) {
                    this.updateFieldFromValue();
                }
            }
        });
        this.updateFieldFromValue();
        this.getSkinnable().requestLayout();
        this.getChildren().add(this.field);
    }

    private void updateValueFromField() throws ParseException {
        if (!this.silent) {
            final String text = this.field.getText();
            final T value = isEmpty(text) ? null : this.assertType(this.getFormat().parse(text));
            this.getSkinnable().setValue(value);
        }
    }

    private T assertType(Number parse) {
        return this.getSkinnable().getNumberConverter().apply(parse);
    }

    private void updateFieldFromValue() {
        if (!this.silent) {
            try {
                this.silent = true;
                this.field.setText(Optional.ofNullable(this.getSkinnable().getValue()).map(this.getFormat()::format).orElse(""));
            } finally {
                this.silent = false;
            }
        }
    }

    public NumberFormat getFormat() {
        return this.getSkinnable().getFormat();
    }

    @Override
    protected void layoutChildren(double contentX, double contentY, double contentWidth, double contentHeight) {
        this.layoutInArea(this.field, contentX, contentY, contentWidth, contentHeight, -1, HPos.CENTER, VPos.CENTER);
    }
}


package eprecise.libs.fx.dataModel;

import java.util.List;
import java.util.Optional;


public interface DataModel<T> {

    default String toLabel(T value) {
        return Optional.ofNullable(value).map(Object::toString).orElse("");
    }

    List<T> search(String text);

}

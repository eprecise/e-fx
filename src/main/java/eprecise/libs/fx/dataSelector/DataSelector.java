
package eprecise.libs.fx.dataSelector;

import eprecise.libs.fx.dataModel.DataModel;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;


public class DataSelector<T> extends Control {

    private final ObjectProperty<T> valueProperty = new SimpleObjectProperty<>();

    private final ObjectProperty<DataModel<T>> dataModelProperty = new SimpleObjectProperty<>();

    private final BooleanProperty showingSugestionsProperty = new SimpleBooleanProperty(false);

    private final StringProperty promptTextProperty = new SimpleStringProperty();

    private final DoubleProperty sugestionScaleProperty = new SimpleDoubleProperty(0.5);

    private final ObjectProperty<EventHandler<ActionEvent>> onActionProperty = new SimpleObjectProperty<EventHandler<ActionEvent>>() {

        @Override
        protected void invalidated() {
            DataSelector.this.setEventHandler(ActionEvent.ACTION, this.get());
        }

        @Override
        public Object getBean() {
            return DataSelector.this;
        }

        @Override
        public String getName() {
            return "onAction";
        }
    };

    private int waitTime = 200;

    public DataSelector() {
        this.getStyleClass().add("data-selector");
        this.setFocusTraversable(false);
    }

    @Override
    protected Skin<?> createDefaultSkin() {
        return new DataSelectorSkin<T>(this);
    }

    public T getValue() {
        return this.valueProperty.get();
    }

    public void setValue(T value) {
        this.valueProperty.set(value);
    }

    public ObjectProperty<T> valueProperty() {
        return this.valueProperty;
    }

    public DataModel<T> getDataModel() {
        return this.dataModelProperty.get();
    }

    public void setDataModel(DataModel<T> dataModel) {
        this.dataModelProperty.set(dataModel);
    }

    public ObjectProperty<DataModel<T>> dataModelProperty() {
        return this.dataModelProperty;
    }

    public boolean isShowingSugestions() {
        return this.showingSugestionsProperty.get();
    }

    public void setShowingSugestions(boolean value) {
        this.showingSugestionsProperty.set(value);
    }

    public BooleanProperty showingSugestionsProperty() {
        return this.showingSugestionsProperty;
    }

    public void showSugestions() {
        this.showingSugestionsProperty.set(true);
    }

    public void hideSugestions() {
        this.showingSugestionsProperty.set(false);
    }

    public void setShowSuggestionWaitTime(int waitTime) {
        this.waitTime = waitTime;
    }

    public int getShowSuggestionWaitTime() {
        return this.waitTime;
    }

    public String getPromptText() {
        return this.promptTextProperty.getValue();
    }

    public void setPromptText(String value) {
        this.promptTextProperty.set(value);
    }

    public StringProperty promptTextProperty() {
        return this.promptTextProperty;
    }

    public double getSugestionScale() {
        return this.sugestionScaleProperty.get();
    }

    public void setSugestionsScale(double value) {
        this.sugestionScaleProperty.set(value);
    }

    public DoubleProperty sugestionScaleProperty() {
        return this.sugestionScaleProperty;
    }

    public EventHandler<ActionEvent> getOnAction() {
        return this.onActionProperty.get();
    }

    public void setOnAction(EventHandler<ActionEvent> value) {
        this.onActionProperty.set(value);
    }

    public ObjectProperty<EventHandler<ActionEvent>> onActionProperty() {
        return this.onActionProperty;
    }
}

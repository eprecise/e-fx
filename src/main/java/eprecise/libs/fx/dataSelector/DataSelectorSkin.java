
package eprecise.libs.fx.dataSelector;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.sun.javafx.util.Utils;

import eprecise.libs.fx.dataModel.DataModel;
import javafx.application.Platform;
import javafx.event.EventTarget;
import javafx.geometry.HPos;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.PopupControl;
import javafx.scene.control.Skin;
import javafx.scene.control.SkinBase;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;


public class DataSelectorSkin<T> extends SkinBase<DataSelector<T>> {

    private final TextField field;

    private final Button button;

    private PopupControl popup;

    private final ListView<T> listView = new PopupListView();

    private boolean detectTextChanged;

    private Timer waitTimer;

    private LoaderTimerTask loaderTimerTask;

    protected DataSelectorSkin(DataSelector<T> control) {
        super(control);
        this.field = new TextField();
        this.field.setFocusTraversable(true);
        this.field.focusedProperty().addListener((ov, old, hasFocus) -> this.field.selectEnd());
        control.focusedProperty().addListener((ov, old, hasFocus) -> {
            if (hasFocus) {
                Platform.runLater(this.field::requestFocus);
            }
        });
        this.button = new Button();
        this.button.setFocusTraversable(false);
        this.button.setText("Find");// TODO trocar por imagem
        StackPane.setAlignment(this.field, Pos.CENTER_LEFT);
        StackPane.setAlignment(this.button, Pos.CENTER_RIGHT);
        this.getChildren().addAll(this.field, this.button);
        this.button.setOnAction(evt -> {
            if (!control.isFocused()) {
                control.requestFocus();
            }
            DataSelectorSkin.this.getSkinnable().showSugestions();
        });
        this.updateTextField();
        control.valueProperty().addListener((ov, o, n) -> this.updateTextField());
        this.field.textProperty().addListener(o -> {
            if (this.detectTextChanged) {
                if (this.waitTimer != null) {
                    this.loaderTimerTask.setObsolete(true);
                    this.waitTimer.cancel();
                    this.waitTimer.purge();
                }

                if (this.field.getText() == null || this.field.getText().trim().isEmpty()) {
                    this.getSkinnable().setValue(null);
                    return;
                }
                if (control.getShowSuggestionWaitTime() >= 0) {
                    this.waitTimer = new Timer("lookupTimer");
                    this.loaderTimerTask = new LoaderTimerTask(this.waitTimer);
                    this.waitTimer.schedule(this.loaderTimerTask, control.getShowSuggestionWaitTime());
                }
            }
        });
        control.showingSugestionsProperty().addListener((ov, o, n) -> {
            if (n) {
                final Point2D p = this.getPrefPopupPosition();

                final Scene scene = this.getSkinnable().getScene();
                if (scene != null) {
                    this.getPopup().show(scene.getWindow(), p.getX(), p.getY());
                }
            } else {
                this.getPopup().hide();
            }
        });

        this.field.promptTextProperty().bind(control.promptTextProperty());
        control.addEventHandler(KeyEvent.KEY_PRESSED, e -> {
            if (e.isControlDown() && e.getCode().equals(KeyCode.SPACE)) {
                if (control.isShowingSugestions()) {
                    control.hideSugestions();
                }
                control.showSugestions();
                e.consume();
            }
        });
        this.field.onActionProperty().bind(control.onActionProperty());
        this.listView.fixedCellSizeProperty().bind(this.field.heightProperty().multiply(control.getSugestionScale()));
    }

    private void updateTextField() {
        this.detectTextChanged = false;
        final DataSelector<T> lookupField = this.getSkinnable();
        if (lookupField.getValue() == null) {
            this.field.setText("");
            this.detectTextChanged = true;
            return;
        }
        final Object value = this.getSkinnable().getDataModel().toLabel(lookupField.getValue());
        if (value != null) {
            this.field.setText(value.toString());
        } else {
            this.field.setText("");
        }
        this.detectTextChanged = true;
    }

    private PopupControl getPopup() {
        if (this.popup == null) {
            this.createPopup();
        }
        return this.popup;
    }

    private void createPopup() {
        this.popup = new PopupControl();
        this.popup.setSkin(new PopupSkin());
        this.popup.setAutoHide(true);
        this.popup.setAutoFix(true);
        this.popup.setHideOnEscape(true);
        this.popup.prefWidthProperty().bind(this.field.widthProperty());
        this.popup.addEventHandler(MouseEvent.MOUSE_CLICKED, evt -> this.getSkinnable().hideSugestions());

        this.listView.setCellFactory(o -> new PropertyListCell<T>(this.getSkinnable().getDataModel()));

        /**
         * Taken from {@link com.sun.javafx.scene.control.skin.ComboBoxListViewSkin}
         */
        this.listView.addEventFilter(MouseEvent.MOUSE_RELEASED, t -> {
            // RT-18672: Without checking if the user is clicking in the
            // scrollbar area of the ListView, the comboBox will hide. Therefore,
            // we add the check below to prevent this from happening.
            final EventTarget target = t.getTarget();
            if (target instanceof Parent) {
                final List<String> s = ((Parent) target).getStyleClass();
                if (s.contains("thumb") || s.contains("track") || s.contains("decrement-arrow") || s.contains("increment-arrow")) {
                    return;
                }
            }
            this.getSkinnable().setValue(DataSelectorSkin.this.listView.getSelectionModel().getSelectedItem());
            this.getSkinnable().hideSugestions();

        });

        this.listView.setOnKeyPressed(t -> {
            if (t.getCode() == KeyCode.ENTER) {
                this.getSkinnable().setValue(DataSelectorSkin.this.listView.getSelectionModel().getSelectedItem());
                this.getSkinnable().hideSugestions();
            } else if (t.getCode() == KeyCode.ESCAPE) {
                this.getSkinnable().hideSugestions();
            }
        });
        this.listView.prefWidthProperty().bind(this.field.widthProperty());
    }

    @SuppressWarnings("restriction")
    private Point2D getPrefPopupPosition() {
        return Utils.pointRelativeTo(this.getSkinnable(), this.listView, HPos.CENTER, VPos.BOTTOM, -7, 0, false);
    }

    @Override
    protected void layoutChildren(double contentX, double contentY, double contentWidth, double contentHeight) {
        final double obw = this.button.prefWidth(-1);
        final double fieldWidth = contentWidth - obw;
        this.layoutInArea(this.field, contentX, contentY, fieldWidth, contentHeight, -1, HPos.CENTER, VPos.CENTER);
        this.layoutInArea(this.button, contentX + fieldWidth, contentY, obw, contentHeight, -1, HPos.CENTER, VPos.CENTER);
    }

    private class LoaderTimerTask extends TimerTask {

        private boolean obsolete = false;

        private final Timer timer;

        public LoaderTimerTask(Timer timer) {
            this.timer = timer;
        }

        public void setObsolete(boolean obsolete) {
            this.obsolete = obsolete;
        }

        @Override
        public void run() {
            if (!this.obsolete) {
                final List<T> data = DataSelectorSkin.this.getSkinnable().getDataModel().search(DataSelectorSkin.this.field.getText());
                Platform.runLater(() -> {
                    if (!LoaderTimerTask.this.obsolete) {
                        DataSelectorSkin.this.listView.getItems().clear();
                        if (!data.isEmpty()) {
                            DataSelectorSkin.this.listView.getItems().addAll(data);
                            DataSelectorSkin.this.getSkinnable().showSugestions();
                        }
                    }
                });
            }
            this.timer.cancel();
            this.timer.purge();
        }
    }

    private final class PopupListView extends ListView<T> {

        @Override
        protected double computeMinHeight(double width) {
            return this.getMinCellSize();
        }

        @Override
        protected double computePrefHeight(double width) {
            final double cellSize = this.getMinCellSize();
            final double ch = this.getItems().size() * cellSize;
            return Math.min(ch, this.computeMaxHeight(width));
        }

        private double getMinCellSize() {
            return this.getFixedCellSize() > 0 ? this.getFixedCellSize() : 45;
        }

        @Override
        protected double computeMaxHeight(double width) {
            return 400;
        }
    }

    private final class PopupSkin implements Skin<DataSelector<T>> {

        @Override
        public DataSelector<T> getSkinnable() {
            return DataSelectorSkin.this.getSkinnable();
        }

        @Override
        public Node getNode() {
            return DataSelectorSkin.this.listView;
        }

        @Override
        public void dispose() {
        }
    }

    static class PropertyListCell<T> extends ListCell<T> {

        private final DataModel<T> dataModel;

        public PropertyListCell(DataModel<T> dataModel) {
            this.dataModel = dataModel;
        }

        @Override
        protected void updateItem(T t, boolean bln) {
            super.updateItem(t, bln);
            if (t != null) {
                final Object value = this.dataModel.toLabel(t);
                if (value != null) {
                    this.setText(value.toString());
                }
            }
        }
    }
}
